import socket
import threading
import rsa
import aes
import sys
from tools import *
import multiprocessing

BACKLOG = 5

lock = multiprocessing.Lock()

class Node:
	"""
	Reperesents the node entity
	"""

	def __init__(self, node_name, private_key):

		self.initNode(node_name, private_key)
		self.writeAvailability()
		
		# initialize the sockets
		self.initSockets()
		
		self.handleConnection()

	def initNode(self, node_name, private_key):
		"""
		Initiation of the node attributes

		:param node_name: the name of the node
		:param private_key: the private key of the node
		"""
		
		self.node_name = node_name
		self.private_key = private_key
		self.public_key = NODES_PUBLIC_KEYS[node_name]
		self.previous = 0
		self.next = 0
		
	def writeAvailability(self):
		"""
		Writes into a file when the node is available
		"""
		global lock

		lock.acquire()
		with open(AVAILABLE_NODES_FILES, "a") as f:
			f.write(self.node_name)
			f.write("\n")
			f.close()
		lock.release()

	def writeUnavailability(self):
		"""
		Writes into a file when the node is not available anymore
		"""
		global lock

		lock.acquire()
		with open(AVAILABLE_NODES_FILES, "r") as f:
			x = [node.strip() for node in f.readlines() if node.strip() not in [self.node_name,""]]
			f.close()
			
		with open(AVAILABLE_NODES_FILES, "w") as f:
			for y in x:
				f.write(y + "\n")
			f.close()
		lock.release()

	def initSockets(self):
		"""
		Creates two TCP/IP sockets
		"""

		self.previousSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		self.previousSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		node_address = (NODE_IP, NODES_PORTS[self.node_name])

		self.previousSocket.bind(node_address)

		# listening for a connection from previous socket
		self.previousSocket.listen(BACKLOG)

	def handleConnection(self):
		"""
		Handle when there is a new connection by a peer or another node.
		"""

		print("---------------------------------")
		print('\nNode {} Waiting for a connection ...\n'.format(self.node_name))
		print("---------------------------------\n")

		self.connection, self.client_address = self.previousSocket.accept()
		print("New connection from {}\n\n".format([node for node, port in NODES_PORTS.items() if port == self.previous]))

		self.writeUnavailability()

		self.previous = self.client_address[1]
		
		# handle the first connection
		self.initChain()

		# create aes key
		self.symetric_key_prev = self.initAesPrev(self.connection)

		self.symetric_key_next = None
		self.initAesNext()

		# one thread per connection
		threading.Thread(target = self.sendToPrevious, args = ()).start() 
		threading.Thread(target = self.sendToNext, args = ()).start()


	def initChain(self):
		"""
		Handle the transmission of the message of the first connection
		"""

		encoded_message = self.connection.recv(MESSAGE_SIZE)
		decoded_message = encoded_message.decode(ENCODING_TYPE,'surrogatepass')
		decoded_message = rsa.decryptString(decoded_message, self.public_key, self.private_key)
		print("Received from {} a message".format([node for node, port in NODES_PORTS.items() if port == self.previous]))

		self.next_port = int(decoded_message[:5])
		decoded_message = decoded_message[5:]

		node_address = (NODE_IP, self.next_port)

		self.next = node_address[1]

		if node_address[1] == TRACKER_PORT:

			header = rsa.encryptString(str(self.node_name), TRACKER_PUBLIC_KEY)

			decoded_message = header + decoded_message

		self.nextSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.nextSocket.connect(node_address) 

		self.nextSocket.send(decoded_message.encode(ENCODING_TYPE,'surrogatepass'))

		#print("Send to next node {} the message {}".format([node for node, port in NODES_PORTS.items() if port == self.next], decoded_message))
	
		message = self.nextSocket.recv(MESSAGE_SIZE)
		self.connection.send(message)

		threading.Thread(target = self.acceptNewConnection, args = ()).start()

	def initPublicKeys(self):
		"""
		Tnitialisation of public keys
		:return: the next public key
		"""

		message = "KEY " + str(self.public_key)
		self.nextSocket.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		if self.next_port != TRACKER_PORT:
			public_key_next = self.portToNodePublicKey(self.next_port)
		else:
			public_key_next = TRACKER_PUBLIC_KEY

		return public_key_next

	def initAesNext(self):
		"""
		Initiation of the aes key with the next node
		"""

		public_key_next = self.initPublicKeys()

		# reception of k1
		message = self.nextSocket.recv(MESSAGE_SIZE)
		message = message.decode(ENCODING_TYPE,'surrogatepass')
		k1 = rsa.decryptString(message, self.public_key, self.private_key)

		# generation and sending of k2
		k2 = aes.generateCipherKey()
		message = matrixToString(k2)
		message = rsa.encryptString(message, public_key_next)
		self.nextSocket.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# reception of the confirmation and verification
		message = self.nextSocket.recv(MESSAGE_SIZE)
		message = message.decode(ENCODING_TYPE,'surrogatepass')
		message = rsa.decryptString(message, self.public_key, self.private_key)

		if message != matrixToString(k2):
			self.nextSocket.send(b'0')
			self.initAesNext()
		else:
			self.nextSocket.send(b'1')

		# generation of the final key
		k1 = stringToMatrix(k1,4,4)
		self.symetric_key_next = xorTwoMatrices(k1,k2)

	def initAesPrev(self, socket):
		"""
		Initiation of the aes key with the previous node
		:param socket: the socket of the node we want to exchange the key
		:return: the aes key
		"""

		message = socket.recv(MESSAGE_SIZE)
		message = message.decode(ENCODING_TYPE,'surrogatepass')

		# reception of the public key
		public_key_prev = [0,0]
		public_key_prev[0] = int(message.split()[1][1:-1]) # Because we have something like:
		public_key_prev[1] = int(message.split()[2][:-1])  #        KEY (xxxxx, xxxxx)

		# generation of k1
		k1 = aes.generateCipherKey()
		message = matrixToString(k1)
		message = rsa.encryptString(message, public_key_prev )
		socket.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# reception of k2
		k2 = socket.recv(MESSAGE_SIZE)
		k2 = k2.decode(ENCODING_TYPE,'surrogatepass')
		k2 = rsa.decryptString(k2, self.public_key, self.private_key )
		
		# sending of the confirmation and verification
		message = rsa.encryptString(k2, public_key_prev )
		socket.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# reception of the confirmation and verification
		message = socket.recv(1)
		if message == b'0':
			return self.initAesPrev(socket)
		
		# XOR
		k2 = stringToMatrix(k2,4,4)

		return xorTwoMatrices(k1,k2)


	def stopConnection(self):
		"""
		Stops all the connections
		"""
		
		self.connection.close()
		self.nextSocket.close()
		self.previousSocket.close()


	def sendToPrevious(self):
		"""
		Allows to send a message to the previous node whenever 
		receiving a message from the next node
		"""

		while True:
			print("Waiting for a message from next")
			message = self.nextSocket.recv(MESSAGE_SIZE)

			if not message: 
				break

			if message.decode(ENCODING_TYPE,'surrogatepass') == "Disconnect":

				try:
					self.writeAvailability()
				
					print("Disconnected")
					self.connection.send(message)
					self.stopConnection()
					self.initNode(self.node_name, self.private_key)
					self.initSockets()
					self.handleConnection()
					break

				except:
					self.stopConnection()
					self.initSockets()
					self.initNode(self.node_name, self.private_key)
					self.handleConnection()
					break

			else:
				message = message.decode(ENCODING_TYPE,'surrogatepass')
				message = aes.decrypt(message, self.symetric_key_next)

				print("Received from next node {} the message {}".format([node for node, port in NODES_PORTS.items() if port == self.next], message))

				if message.split()[0] == "CONNECT":

					newSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
					node_address = (NODE_IP, NODES_PORTS[message.split()[2]])
					newSocket.connect(node_address)
					self.nextSocket = newSocket
					self.next_port = node_address[1]

					self.initAesNext()

					request = "SEND THE FILE : " + message.split()[3]
					print(request)
					request = aes.encrypt(request, self.symetric_key_next)
					self.nextSocket.send(request.encode(ENCODING_TYPE, "surrogatepass"))

				else:
					message = aes.encrypt(message, self.symetric_key_prev)
					message = message.encode(ENCODING_TYPE,'surrogatepass')
					self.connection.send(message)
	

	def sendToNext(self):
		"""
		Allows to send a message to the next node whenever 
		receiving a message from the previous node
		"""

		while True:

			print("Waiting for a message from previous")
			message = self.connection.recv(MESSAGE_SIZE)

			if not message: 
				break

			if message.decode(ENCODING_TYPE,'surrogatepass') == "Disconnect":

				try:
					self.writeAvailability()
				
					print("Disconnected")
					self.nextSocket.send(message)
					
					self.stopConnection()

					self.initNode(self.node_name, self.private_key)
					self.initSockets()
					self.handleConnection()
					break
				except:

					self.stopConnection()
					self.initNode(self.node_name, self.private_key)
					self.initSockets()
					self.handleConnection()
					break
			else:
				print("Received from node {} the message : {}".format(self.previous, message.decode(ENCODING_TYPE,'surrogatepass')))
				print("Send to Next")
				message = message.decode(ENCODING_TYPE,'surrogatepass')
				message = aes.decrypt(message, self.symetric_key_prev)
				print(message)
				message = aes.encrypt(message, self.symetric_key_next)
				message = message.encode(ENCODING_TYPE,'surrogatepass')
				self.nextSocket.send(message)


	def acceptNewConnection(self):
		"""
		Waits for another node to connect in the case where the
		tracker asked to bind two nodes together
		"""
		try:
			new_connection, new_client_adress = self.previousSocket.accept()
			message = "Disconnect"
			self.nextSocket.send(message.encode(ENCODING_TYPE, "surrogatepass"))
			self.nextSocket = new_connection

			self.next = new_client_adress[1]
			self.symetric_key_next = self.initAesPrev(self.nextSocket)
			print("Two exit nodes connected")
			self.sendToPrevious()
		except:
			print("Exiting thread accept")


	def portToNodePublicKey(self,port):
		"""
		Returns the public key of the node having a specific port

		:param: the node port
		:return: the public key of the node
		"""
		for i in list(NODES_PORTS.keys()):
			if NODES_PORTS[i] == port:
				return NODES_PUBLIC_KEYS[i]


if __name__ == "__main__":

	if (len(sys.argv)) < 2:
		
		print("Usage : python node.py <node name> <private key>")

	else:

		node = Node(sys.argv[1], int(sys.argv[2]))