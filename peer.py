import socket
import sys
import os
import rsa
import aes
from tools import *

PEER_IP = 'localhost'

class Peer:
	"""
	Represents the peer entity
	"""

	def __init__(self, owned_files_path):

		self.owned_files = os.listdir(owned_files_path)
		self.available_files = []
		self.chosen_nodes = []

		self.symetric_key = None
		
		# initialise the socket
		self.initSocket()

		# establish a connection to the tracker
		self.establish_connection()

		# begin creations of aes keys in the network
		self.initAesNext()

		# handle peer requests
		self.handlePeerRequests()


	def initSocket(self):
		"""
		Creates a TCP/IP socket
		"""

		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)


	def chooseNNodes(self,n):
		"""
		Returns a list of n nodes sorted in ascending order
		in relation to the value of the public key (n, that is public_key[0])

		:param n: the number of nodes to choose
		:return: the chosen nodes
		"""

		chosen_nodes = sample(list(NODES_PORTS.keys()), n)

		f = open(AVAILABLE_NODES_FILES, "r")
		available_nodes = [chosen_node.strip() for chosen_node in f.readlines() if chosen_node != "\n"]
		f.close()

		res = []

		if len(available_nodes) >= n:

			chosen_nodes = sample(available_nodes, n)
			
			lst_n = []

			for chosen_node in chosen_nodes:

				lst_n.append(NODES_PUBLIC_KEYS[chosen_node][0])

			while len(chosen_nodes) > 0:

				mm = min(lst_n)
				index = lst_n.index(mm)
				res.append(chosen_nodes[index])
				chosen_nodes.remove(chosen_nodes[index])
				lst_n.remove(mm)

		print("Chosen nodes : {}".format(res))

		return res


	def createMessageOfOwnedFiles(self):
		"""
		Creates the message with owned files in the correct format

		:return: the message containing the owned files
		"""

		message = ""

		for owned_file in self.owned_files:

			message += owned_file + " "

		message = message.strip()

		print("Sending {}".format(message))

		message = rsa.encryptString(message, TRACKER_PUBLIC_KEY)

		message = str(TRACKER_PORT) + message

		for chosen_node in self.chosen_nodes:

			message = rsa.encryptString(message, NODES_PUBLIC_KEYS[chosen_node])
			message = str(NODES_PORTS[chosen_node]) + message

		return message[5:]
		

	def establish_connection(self):
		"""
		Establish a connection to the tracker by choosing tree nodes
		"""

		while True:

			# choose 'randomly' tree nodes
			self.chosen_nodes = self.chooseNNodes(3)

			if len(self.chosen_nodes) == 3: break

		# establish the connection with the first node
		node_address = (PEER_IP, NODES_PORTS[self.chosen_nodes[-1]])

		print('Connecting to {} on port {} ...'.format(*node_address))

		# connection
		self.sock.connect(node_address)

		# creation of the message in the correct format
		message = self.createMessageOfOwnedFiles()

		# sending the list of owned files
		self.sock.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# receiving the list of all the files available through the network
		encoded_available_files = self.sock.recv(MESSAGE_SIZE)

		decoded_available_files = encoded_available_files.decode(ENCODING_TYPE,'surrogatepass')

		self.available_files = decoded_available_files.split(" ")


	def initAesNext(self):
		"""
		Initiation of the aes key with the next node
		"""

		# sending of the public key
		rsa_keys = rsa.keygen()
		message = "KEY " + str(rsa_keys[0])
		self.sock.send(message.encode(ENCODING_TYPE,'surrogatepass'))
		public_key_next = NODES_PUBLIC_KEYS[self.chosen_nodes[-1]]

		# reception of k1
		message = self.sock.recv(MESSAGE_SIZE)
		message = message.decode(ENCODING_TYPE,'surrogatepass')
		k1 = rsa.decryptString(message, rsa_keys[0], rsa_keys[1])


		# generation and sending of k2
		k2 = aes.generateCipherKey()
		message = matrixToString(k2)
		message = rsa.encryptString(message, public_key_next)
		self.sock.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# reception of the confirmation and verification
		message = self.sock.recv(MESSAGE_SIZE)
		message = message.decode(ENCODING_TYPE,'surrogatepass')
		message = rsa.decryptString(message, rsa_keys[0], rsa_keys[1])

		if message != matrixToString(k2):
			self.sock.send(b'0')
			self.initAesNext()
		else:
			self.sock.send(b'1')

		# generation of the final key
		k1 = stringToMatrix(k1,4,4)
		self.symetric_key = xorTwoMatrices(k1,k2)


	def handlePeerRequests(self):
		"""
		Handle the peer request
		"""

		while True:

			print("1) Download file")
			print("2) Seed")
			print("3) Quit\n")

			choice = input()

			if(choice == "1"): # dwonload
				
				cls();

				while True:
					print("--------------------------------------------")
					print("Available files through the network : {}\n".format([file for file in self.available_files if file not in self.owned_files]))
					print("Select the file you would like to download :")

					selected_file = input()

					if selected_file in self.available_files:
						break
					else:
						print("\nPlease select an existing file name !\n")
						print("--------------------------------------------")

				print("\nDownloading '{}' file ...".format(selected_file))

				selected_file = aes.encrypt(selected_file, self.symetric_key)
				selected_file = selected_file.encode(ENCODING_TYPE,'surrogatepass')
				self.sock.send(selected_file)
				message = self.sock.recv(MESSAGE_SIZE)
				message = message.decode(ENCODING_TYPE, "surrogatepass")
				message = aes.decrypt(message, self.symetric_key)
				print("The file '{}' was successfully downloaded !".format(message))
				
				break

			elif(choice == "2"): # waiting to give to someone his files

				request = self.sock.recv(MESSAGE_SIZE)
				request = request.decode(ENCODING_TYPE, "surrogatepass")
				request = aes.decrypt(request, self.symetric_key)
				message = "Salut les boys"
				message = aes.encrypt(message, self.symetric_key)
				self.sock.send(message.encode(ENCODING_TYPE, "surrogatepass"))

				break

			elif(choice == "3"): # quit

				break

		message = "Disconnect"
		self.sock.send(message.encode(ENCODING_TYPE,'surrogatepass'))
		print("Sent Disconnect")
		self.sock.close()
	  

def cls():
	"""
	Clears the command line according to the operating system
	"""
	
	os.system('cls' if os.name=='nt' else 'clear')


if __name__ == "__main__":

	peer = Peer(sys.argv[1])