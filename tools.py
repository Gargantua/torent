from random import randint, sample

NODE_IP = 'localhost'

TRACKER_IP = 'localhost'
TRACKER_PORT = 10002
TRACKER_PUBLIC_KEY = (18923,15971)

NODES_PORTS = {"A" : 11005, "B" : 14005, "C": 13005, "D" : 16005, "E" : 17005, "F" : 18005}
NODES_PUBLIC_KEYS = {"A" : (96091,71923), "B" : (24257,23713), "C": (35263,23319), "D" : (72731,67505), "E" : (34933,25159), "F" : (22879,14145)}

MESSAGE_SIZE = 4096
ENCODING_TYPE = "UTF-32"

AVAILABLE_NODES_FILES = "availableNodes.txt"

def matrixToString(matrix):
	"""
	Transforms the matrix into a string

	:param matrix: the matrix
	:return: the resulting string
	"""
	string = ""

	for i in range(len(matrix)):
		for j in range (len(matrix[i])):
			if len(str(matrix[i][j])) == 1:
				string += '0'
			string += str(matrix[i][j])

	return string

def stringToMatrix(string, n, m):
	"""
	Transforms the string into a matrix

	:param string: the string to be transformed
	:param n: number of columns
	:param m: number of lines
	:return: the resulting matrix
	"""

	matrix = []
	tmp = []

	for i in range(m):

		for j in range (0,n*2,2):  # times two because of the size of one cell in the matrix: two characters
			tmp.append(string[(i*n*2)+j:(i*n*2)+j+2])

		matrix.append(tmp)
		tmp = []

	return matrix


def xorTwoMatrices(m1,m2):
	"""
	Returns the xor of two matrix of same size

	:param m1: the first matrix
	:param m2: the second matrix
	:return: the xor of the two matrix
	"""

	matrix = []

	for i in range(len(m1)):

		matrix.append([])

		for j in range(len(m2)):

			matrix[i].append( hex(int(m1[i][j], 16) ^ int(m2[i][j], 16))[2:] )

	return matrix