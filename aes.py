import numpy
from random import randint

# CONSTANTS
GALOIS_FIELD_MATRIX = [["02", "03", "01", "01"],
						  ["01", "02", "03", "01"],
						  ["01", "01", "02", "03"],
						  ["03", "01", "01", "02"]]

INV_GALOIS_FIELD_MATRIX = [["14", "11", "13", "9"],
							 ["9", "14", "11", "13"],
							 ["13", "9", "14", "11"],
							 ["11", "13", "9", "14"]]

SBOX = [
	[0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76],
	[0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0],
	[0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15],
	[0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75],
	[0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84],
	[0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF],
	[0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8],
	[0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2],
	[0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73],
	[0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB],
	[0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79],
	[0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08],
	[0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A],
	[0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E],
	[0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF],
	[0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16]]

INVERSE_SBOX = [
	[0x52, 0x09, 0x6A, 0xD5, 0x30, 0x36, 0xA5, 0x38, 0xBF, 0x40, 0xA3, 0x9E, 0x81, 0xF3, 0xD7, 0xFB],
	[0x7C, 0xE3, 0x39, 0x82, 0x9B, 0x2F, 0xFF, 0x87, 0x34, 0x8E, 0x43, 0x44, 0xC4, 0xDE, 0xE9, 0xCB],
	[0x54, 0x7B, 0x94, 0x32, 0xA6, 0xC2, 0x23, 0x3D, 0xEE, 0x4C, 0x95, 0x0B, 0x42, 0xFA, 0xC3, 0x4E],
	[0x08, 0x2E, 0xA1, 0x66, 0x28, 0xD9, 0x24, 0xB2, 0x76, 0x5B, 0xA2, 0x49, 0x6D, 0x8B, 0xD1, 0x25],
	[0x72, 0xF8, 0xF6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xD4, 0xA4, 0x5C, 0xCC, 0x5D, 0x65, 0xB6, 0x92],
	[0x6C, 0x70, 0x48, 0x50, 0xFD, 0xED, 0xB9, 0xDA, 0x5E, 0x15, 0x46, 0x57, 0xA7, 0x8D, 0x9D, 0x84],
	[0x90, 0xD8, 0xAB, 0x00, 0x8C, 0xBC, 0xD3, 0x0A, 0xF7, 0xE4, 0x58, 0x05, 0xB8, 0xB3, 0x45, 0x06],
	[0xD0, 0x2C, 0x1E, 0x8F, 0xCA, 0x3F, 0x0F, 0x02, 0xC1, 0xAF, 0xBD, 0x03, 0x01, 0x13, 0x8A, 0x6B],
	[0x3A, 0x91, 0x11, 0x41, 0x4F, 0x67, 0xDC, 0xEA, 0x97, 0xF2, 0xCF, 0xCE, 0xF0, 0xB4, 0xE6, 0x73],
	[0x96, 0xAC, 0x74, 0x22, 0xE7, 0xAD, 0x35, 0x85, 0xE2, 0xF9, 0x37, 0xE8, 0x1C, 0x75, 0xDF, 0x6E],
	[0x47, 0xF1, 0x1A, 0x71, 0x1D, 0x29, 0xC5, 0x89, 0x6F, 0xB7, 0x62, 0x0E, 0xAA, 0x18, 0xBE, 0x1B],
	[0xFC, 0x56, 0x3E, 0x4B, 0xC6, 0xD2, 0x79, 0x20, 0x9A, 0xDB, 0xC0, 0xFE, 0x78, 0xCD, 0x5A, 0xF4],
	[0x1F, 0xDD, 0xA8, 0x33, 0x88, 0x07, 0xC7, 0x31, 0xB1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xEC, 0x5F],
	[0x60, 0x51, 0x7F, 0xA9, 0x19, 0xB5, 0x4A, 0x0D, 0x2D, 0xE5, 0x7A, 0x9F, 0x93, 0xC9, 0x9C, 0xEF],
	[0xA0, 0xE0, 0x3B, 0x4D, 0xAE, 0x2A, 0xF5, 0xB0, 0xC8, 0xEB, 0xBB, 0x3C, 0x83, 0x53, 0x99, 0x61],
	[0x17, 0x2B, 0x04, 0x7E, 0xBA, 0x77, 0xD6, 0x26, 0xE1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0C, 0x7D]]

RCON = [
	[0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36],
	[0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
	[0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
	[0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]]

##############################################################################################################################

def pad(message):
	"""
	This method fills (pad) the text to encrypt on a text with a length of multiple of 16

	:param message: message to pad
	:return: the padded message
	"""
	
	num = 16 - len(message) % 16
	for i in range(num):
		message += " "

	message += str(num)

	if num > 9:
		secondPadLength = 14
	else: 
		secondPadLength = 15
	
	for i in range(secondPadLength):
		message += " "

	return message


def unpad(message):
	"""
	This method unpad the decrypted text

	:param message: message to unpad
	:return: the unpadded message
	"""

	num = int(message[-16::])
	totalPad = 16 + num
	message = message[:-totalPad]

	return message

def intColumnToHexColumn(column):
	"""
	This method converts a column of integers into a column of hexadecimal numbers

	:param column: the column to be converted
	:return: the converted column
	"""

	for i in range(len(column)):
		column[i] = hex(column[i])[2:]

	return column

def textToMatrix(text):
	"""
	This method converts a block of 16 bytes into a matrix composed of hexadecimal numbers

	:param text: the text to convert into matrix
	:return: the matrix of the text
	"""

	matrix = []
	row = []

	for i in range(16):

		if (i + 1) % 4 != 0:
			row.append(hex(ord(text[i]))[2:])
		else:
			row.append(hex(ord(text[i]))[2:])
			matrix.append(row)
			row = []

	return matrix

def matrixToText(matrix):
	"""
	Converts a matrix of hexadecimal numbers into text

	:param matrix: matrix to be converted
	:return: the text of the matrix
	"""

	string = ""

	for i in range(len(matrix)):
		for j in range(len(matrix)):

			matrix[i][j] = int(matrix[i][j], 16)
			string += chr(matrix[i][j])

	return string

def xorHexadecimals(*hexadecimalNumbers):
	"""
	This method computes the xor operation between hexadecimal values

	:param hexadecimalNumbers : the hexadecimal numbers to xor
	:return: the result of the xor between the numbers
	"""

	computedXor = int(hexadecimalNumbers[0], 16)

	for i in range(1, len(hexadecimalNumbers)):

		computedXor = (computedXor ^ int(hexadecimalNumbers[i], 16))

	return hex(computedXor)[2:]

def xorTwoBinary(binaryNumber1, binaryNumber2):
	"""
	This method computes the xor operation between two binary values

	:param binaryNumber1: the first binary number
	:param binaryNumber2: the second binary number
	:return: the result of the xor between the 2 numbers
	"""
	return bin(int(binaryNumber1, 2) ^ int(binaryNumber2, 2))[2:].zfill(8)

def xorFourBinaryToHex(binNumber1, binNumber2, binNumber3, binNumber4):
	"""
	This method computes the xor operation between four binary values

	:param binNumber1: first binary number
	:param binNumber2: second binary number
	:param binNumber3: third binary number
	:param binNumber4: fourth binary number
	:return: result of xor between the 4 numbers
	"""

	return hex(int(binNumber1, 2) ^ int(binNumber2, 2) ^ int(binNumber3, 2) ^ int(binNumber4, 2))[2:]

def xorTwoColumns(column1, column2):
	"""
	This method computes the xor operation between two columns and then returns the resulting column

	:param column1: first column
	:param column2: second column
	:return: xor result between the 2 columns
	"""

	column = []

	for i in range(len(column1)):

		column.append(xorHexadecimals(column1[i], column2[i]))

	return column

def xorThreeColumns(column1, column2, column3):
	"""
	This method computes the xor operation between two columns and then returns the resulting column

	:param column1: first column
	:param column2: second column
	:param column3: third column
	:return: xor result between the 3 columns
	"""

	column = []

	for i in range(len(column1)):
		column.append(xorHexadecimals(column1[i], column2[i], column3[i]))

	return column

def getMatrixColumn(matrix, columnNumber):
	"""
	Returns the values of a matrix's column

	:param matrix: matrix to manage
	:param columnNumber: the column numer
	:return: the values of the column
	"""

	column = []

	for i in range(len(matrix)):
		column.append(matrix[i][columnNumber])

	return column

def setMatrixColumn(matrix, column, columnNumber):
	"""
	This method fills a column of a matrix

	:param matrix: matrix to manage
	:param column: the column to manage
	:param columnNumber: the column number to set
	:return: the filled matrix
	"""

	for i in range(len(matrix)):
		matrix[i][columnNumber] = column[i]

	return matrix

def hexadecimalWithTwoDigits(column):
	"""
	This method force each hexadecimal value to have 2 digits

	:param column: the column to manage
	:return: the new column containing 2 digits hexadecimal values
	"""

	for i in range(len(column)):

		if len(column[i]) < 2:
			column[i] = "0" + column[i]

	return column

def subBytes(column, isInverse):
	"""
	This method computes the subBytes operation

	:param column: column to be replaced by the new sBox values
	:return: the new column
	"""

	correspondanceDict = {"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9,
						  "a": 10, "b": 11, "c": 12, "d": 13, "e": 14, "f": 15
						  }
	newColumn = []
	column = hexadecimalWithTwoDigits(column)

	for element in column:

		if(isInverse):
			newHexValue = hex(INVERSE_SBOX[correspondanceDict[element[0]]][correspondanceDict[element[1]]])[2:]
		else:
			newHexValue = hex(SBOX[correspondanceDict[element[0]]][correspondanceDict[element[1]]])[2:]
		
		newColumn.append(newHexValue)

	return newColumn

def shiftRows(matrix, isInverse):
	"""
	This method rotates each line (excepts the first line) with an offset of i (number of the line)

	:param matrix: matrix to be rotated
	:return: the matrix with the rows rotated
	"""

	for i in range(len(matrix)):

		if i:
			if(isInverse):
				matrix[i] = list(numpy.roll(matrix[i], i))
			else:
				matrix[i] = list(numpy.roll(matrix[i], -i))

	return matrix

def mixColumns(matrix, isInverse):
	"""
	This method computes the MixColumn operation

	:param matrix: matrix after the shift rows operation
	:return: the matrix after the mix columns operation
	"""

	finalColumn = []
	finalMatrix = [[0 for i in range(4)] for j in range(4)]

	for k in range(len(matrix)):

		column = getMatrixColumn(matrix, k)

		if(isInverse):
			for i in range(len(INV_GALOIS_FIELD_MATRIX)):
				listOfComputedBinValues = []

				for j in range(len(column)):
					binaryValue = bin(int(column[j], 16))[2:].zfill(8)
					binaryValueShifted = binaryValue[1:] + "0"

					if INV_GALOIS_FIELD_MATRIX[i][j] == "9":
						listOfComputedBinValues.append(manageCase9(binaryValue, binaryValueShifted).zfill(8))


					elif INV_GALOIS_FIELD_MATRIX[i][j] == "11":
						listOfComputedBinValues.append(manageCase11(binaryValue, binaryValueShifted).zfill(8))


					elif INV_GALOIS_FIELD_MATRIX[i][j] == "13":
						listOfComputedBinValues.append(manageCase13(binaryValue, binaryValueShifted).zfill(8))

					elif INV_GALOIS_FIELD_MATRIX[i][j] == "14":
						listOfComputedBinValues.append(manageCase14(binaryValue, binaryValueShifted).zfill(8))

				finalValue = xorFourBinaryToHex(listOfComputedBinValues[0], listOfComputedBinValues[1],
													 listOfComputedBinValues[2], listOfComputedBinValues[3])
				finalColumn.append(finalValue)

		else:
			for i in range(len(GALOIS_FIELD_MATRIX)):
			# for each line of the galois field matrix

				listOfComputedBinValues = []

				for j in range(len(column)):
					# for each value in the column

					binaryValue = bin(int(column[j], 16))[2:].zfill(8)
					binaryValueShifted = binaryValue[1:] + "0"

					if GALOIS_FIELD_MATRIX[i][j] == "01":
						listOfComputedBinValues.append(binaryValue)

					elif GALOIS_FIELD_MATRIX[i][j] == "02":

						listOfComputedBinValues.append(manageCase02(binaryValue, binaryValueShifted))

					elif GALOIS_FIELD_MATRIX[i][j] == "03":

						if binaryValue[0] == "1":
							binaryValueComputed = bin(int(binaryValueShifted, 2) ^ int("00011011", 2))[2:].zfill(8)
							listOfComputedBinValues.append(
								xorTwoBinary(binaryValueComputed,binaryValue).zfill(8))

						else:
							listOfComputedBinValues.append(
								xorTwoBinary(binaryValueShifted, binaryValue).zfill(8))

				finalValue = xorFourBinaryToHex(listOfComputedBinValues[0], listOfComputedBinValues[1],
													 listOfComputedBinValues[2], listOfComputedBinValues[3])
				finalColumn.append(finalValue)

		finalMatrix = setMatrixColumn(finalMatrix, finalColumn, k)
		finalColumn = []

	return finalMatrix


def manageCase02(binaryValue, binaryValueShifted):
	"""
	Managing the case when a 02 appears in the Galois field matrix for the Mix Column step

	:param binaryValue: a binary value
	:param binaryValueShifted: the binary value shifted
	:return: the result of the computation
	"""

	if binaryValue[0] == "1":
		computedBinaryValue = (xorTwoBinary(binaryValueShifted, "00011011").zfill(8))
	else:
		computedBinaryValue = (bin(int(binaryValueShifted, 2))[2:].zfill(8))

	return computedBinaryValue

def manageCase9(binaryValue, binaryValueShifted):
	"""
	Managing the case when a 09 appears in the inverse Galois field matrix for the inverse Mix Column step

	:param binaryValue: a binary value
	:param binaryValueShifted: the binary value shifted
	:return: the result of the computation
	"""

	temporaryBinValue = binaryValue
	temporaryShiftedBinValue = binaryValueShifted

	for i in range(3):

		computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)
		temporaryBinValue = computedBinaryValue
		temporaryShiftedBinValue = temporaryBinValue[1:] + "0"

	return xorTwoBinary(computedBinaryValue, binaryValue)

def manageCase11(binaryValue, binaryValueShifted):
	"""
	Managing the case when a 11 appears in the inverse Galois field matrix for the inverse Mix Column step

	:param binaryValue: a binary value
	:param binaryValueShifted: the binary value shifted
	:return: the result of the computation
	"""

	temporaryBinValue = binaryValue
	temporaryShiftedBinValue = binaryValueShifted

	# Case 2 two times
	for i in range(2):

		computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)
		temporaryBinValue = computedBinaryValue
		temporaryShiftedBinValue = temporaryBinValue[1:] + "0"

	computedBinaryValue = xorTwoBinary(computedBinaryValue, binaryValue)
	temporaryBinValue = computedBinaryValue
	temporaryShiftedBinValue = temporaryBinValue[1:] + "0"
	computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)
	computedBinaryValue = xorTwoBinary(computedBinaryValue, binaryValue)

	return computedBinaryValue

def manageCase13(binaryValue, binaryValueShifted):
	"""
	Managing the case when a 13 appears in the inverse Galois field matrix for the inverse Mix Column step

	:param binaryValue: a binary value
	:param binaryValueShifted: the binary value shifted
	:return: the result of the computation
	"""

	temporaryBinValue = binaryValue
	temporaryShiftedBinValue = binaryValueShifted

	computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)

	computedBinaryValue = xorTwoBinary(computedBinaryValue, binaryValue)

	temporaryBinValue = computedBinaryValue
	temporaryShiftedBinValue = computedBinaryValue[1:] + "0"

	# Case 2 two times
	for i in range(2):

		computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)

		temporaryBinValue = computedBinaryValue
		temporaryShiftedBinValue = temporaryBinValue[1:] + "0"

	computedBinaryValue = xorTwoBinary(computedBinaryValue, binaryValue)

	return computedBinaryValue

def manageCase14(binaryValue, binaryValueShifted):
	"""
	Managing the case when a 14 appears in the inverse Galois field matrix for the inverse Mix Column step

	:param binaryValue: a binary value
	:param binaryValueShifted: the binary value shifted
	:return: the result of the computation
	"""

	temporaryBinValue = binaryValue
	temporaryShiftedBinValue = binaryValueShifted

	computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)
	computedBinaryValue = xorTwoBinary(computedBinaryValue, binaryValue)

	temporaryBinValue = computedBinaryValue
	temporaryShiftedBinValue = temporaryBinValue[1:] + "0"

	computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)

	computedBinaryValue = xorTwoBinary(computedBinaryValue, binaryValue)

	temporaryBinValue = computedBinaryValue
	temporaryShiftedBinValue = temporaryBinValue[1:] + "0"
	computedBinaryValue = manageCase02(temporaryBinValue, temporaryShiftedBinValue)

	return computedBinaryValue



def computeRoundKeys(cipherKeyMatrix):
	"""
	This method computes all the round keys from the cipherkey matrix

	:return: the extended cipher key
	"""

	roundKeysDict = {}
	roundKeysDict[0] = cipherKeyMatrix

	for i in range(1, 11):
	# for each roundkey:

		roundKeyMatrix = [[0 for i in range(4)] for j in range(4)]
		roundKeysDict[i] = roundKeyMatrix

		for j in range(4):
		# for each column of a roundkey:

			if not j:
			# special case for the first column

				w_i = getMatrixColumn(roundKeysDict[i - 1],3)  # we take the last column from the previous roundkey
				w_i = numpy.roll(w_i, -1)  # we rotate the column
				w_i = subBytes(w_i,False)  # we apply SubBytes on it
				w_i_4 = getMatrixColumn(roundKeysDict[i - 1],0)  # we take the first column from the previous roundkey
				rconColumn = intColumnToHexColumn(getMatrixColumn(RCON,i - 1))  # we take the i th (round number) column from the rcon matrix
				firstColumn = xorThreeColumns(w_i, w_i_4,rconColumn)  # we compute the XOR between the three columns
				firstColumn = hexadecimalWithTwoDigits(firstColumn)  # be sure that all the value is composed of 2 digits before setting in the matrix
				roundKeyMatrix = setMatrixColumn(roundKeyMatrix, firstColumn,0)  # set the colum on the roundkey number i
				equivalentColumn = firstColumn  # this column represent the j th column in the previous roundkey

			else:
				column = getMatrixColumn(roundKeysDict[i - 1],j)  # we take the j th column from the previous roundkey
				resultColumn = xorTwoColumns(column, equivalentColumn)
				resultColumn = hexadecimalWithTwoDigits(resultColumn)  # be sure that all the value is composed of 2 digits before setting in the matrix
				roundKeyMatrix = setMatrixColumn(roundKeyMatrix, resultColumn,j)  # set the new column on the roundkey number i
				equivalentColumn = resultColumn

		roundKeysDict[i] = roundKeyMatrix

	return roundKeysDict

def addRoundKey(stateMatrix, roundKeyMatrix):
	"""
	This method computes the add roundkey operation between a state matrix and the roundkey matrix

	:param stateMatrix: the matrix after the mix column operation
	:param roundKeyMatrix: the matrix containing all the sub keys
	:return: the matrix after the add round key operation
	"""

	newStateMatrix = [[0 for i in range(4)] for j in range(4)]

	for i in range(len(roundKeyMatrix)):
		for j in range(len(stateMatrix)):

			hexValue = xorHexadecimals(stateMatrix[i][j], roundKeyMatrix[i][j])

			if len(hexValue) < 2:
				hexValue = "0" + hexValue

			newStateMatrix[i][j] = hexValue

	return newStateMatrix

def stringToBlock(paddedString):
	"""
	This method separates the string into a list of states matrix

	:param paddedString: the string to separate
	:return: list of states matrix from the string 
	"""

	blockList = []
	begin = 0
	end = 16

	for i in range(len(paddedString) // 16):

		blockList.append(textToMatrix(paddedString[begin:end]))
		begin += 16
		end += 16

	return blockList

def encrypt(message, cipherKey):
	"""
	This method encypts the plain text

	:return: the encrypted text
	"""

	roundKeysDict = computeRoundKeys(cipherKey)
	cipherText = ""
	stringToEncrypt = pad(message)
	blockList = stringToBlock(stringToEncrypt)

	for l in range(len(blockList)):
	# initial round
		newStateMatrix = addRoundKey(blockList[l], cipherKey)

		# 10 rounds of encryption:
		for i in range(1, 11):

			# SubBytes operation
			for k in range(len(newStateMatrix)):
				stateColumn = getMatrixColumn(newStateMatrix, k)
				stateColumn = subBytes(stateColumn,False)
				newStateMatrix = setMatrixColumn(newStateMatrix, stateColumn, k)

			# ShiftRows operation
			newStateMatrix = shiftRows(newStateMatrix,False)

			if i != 10:
				# MixColumns operation
				newStateMatrix = mixColumns(newStateMatrix,False)

			# Add roundkey operation
			newStateMatrix = addRoundKey(newStateMatrix, roundKeysDict[i])

		cipherText += matrixToText(newStateMatrix)
	
	return cipherText

def decrypt(encryptedText, cipherKey):
	"""
	This method decrypts the ciphertext

	:return: the decrypted ciphertex
	"""

	roundKeysDict = computeRoundKeys(cipherKey)

	decryptedText = ""
	blockList = stringToBlock(encryptedText)

	for l in range(len(blockList)):

		# initial round
		newCipherState = addRoundKey(blockList[l], roundKeysDict[10])

		# inverse shift row operation
		newCipherState = shiftRows(newCipherState,True)

		# inverse SubBytes operation
		for k in range(len(newCipherState)):
			stateColumn = getMatrixColumn(newCipherState, k)
			stateColumn = subBytes(stateColumn,True)
			newCipherState = setMatrixColumn(newCipherState, stateColumn, k)

		for i in range(9, 0, -1):

			# add roundkey operation
			newCipherState = addRoundKey(newCipherState, roundKeysDict[i])

			newCipherState = mixColumns(newCipherState,True)

			# inverse shift row operation
			newCipherState = shiftRows(newCipherState,True)

			# inverse SubBytes operation
			for k in range(len(newCipherState)):
				stateColumn = getMatrixColumn(newCipherState, k)
				stateColumn = subBytes(stateColumn,True)
				newCipherState = setMatrixColumn(newCipherState, stateColumn, k)

		newCipherState = addRoundKey(newCipherState, cipherKey)
		decryptedText += matrixToText(newCipherState)

	return unpad(decryptedText)


def encryptFile(fileName, cipherKey):
	"""
	Encrypts the content of a file

	:param fileName: file's name to encrypt
	:param cipherKey: the cipher key
	:return:  the encrypted message (that was in file)
	"""

	stringToEncrypt = readFile(fileName)
	encryptedString = encrypt(stringToEncrypt, cipherKey)
	return encryptedString

def decryptAndWriteFile(encryptedString, fileName, cipherKey):
	"""
	Decrypts the content of a message and writes it in a file

	:param encryptedString: the encrypted message 
	:param fileName: the file name where the decrypted content will be written
	:param cipherKey: the cipher key
	"""

	decryptedString = decrypt(encryptedString, cipherKey)
	stringToFile(decryptedString, fileName)


def generateCipherKey():
	"""
	Generates a matrix composed with random hexadecimal values

	:return: random cipher key
	"""

	return [[hex(randint(0, (2 ** 8) - 1))[2:] for i in range(4)] for j in range(4)]


def readFile(myFile):
	"""
	Reads a file to transform it into a string

	:param myFile: file name 
	:return: content of the file
	"""

	resultingString = ""
	with open(myFile) as f:
		allLines = f.readlines()

	for line in allLines:
		resultingString += line

	f.close()

	return resultingString


def stringToFile(myString, fileName):
	"""
	Writes a string in a file

	:param myString: string to write in fileName
	:param fileName: file name
	"""

	fileToWrite = open(fileName, 'w')
	fileToWrite.write(myString)
	fileToWrite.close()