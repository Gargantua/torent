#!/bin/bash

> "availableNodes.txt"
xterm -geometry 150×32 -hold -e python3 tracker.py 2459&
xterm -geometry 150×32 -hold -e python3 node.py A 25963&
xterm -geometry 150×32 -hold -e python3 node.py B 8437&
xterm -geometry 150×32 -hold -e python3 node.py C 32575&
xterm -geometry 150×32 -hold -e python3 node.py D 36689&
xterm -geometry 150×32 -hold -e python3 node.py E 32119&
xterm -geometry 150×32 -hold -e python3 node.py F 2177&
xterm -geometry 150×32 -hold &
xterm -geometry 150×32 -hold &
