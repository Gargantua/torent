import socket
import threading
import sys
import rsa
import aes
from tools import *

BACKLOG = 5

class Tracker():
	"""
	Represents the tracker entity
	"""

	def __init__(self, private_key):

		self.private_key = private_key
		self.public_key = TRACKER_PUBLIC_KEY

		self.available_files = {}

		# socket initialisation
		self.initSocket()

		# accept connections
		while True:
		 
			print('Waiting for a connection ...\n')

			connection, client_address = self.sock.accept()

			print('New connection !\n')

			# each thread will handle a connection
			threading.Thread(target = self.handlePeerRequest, args = (connection , client_address)).start()


	def initSocket(self):
		"""
		Creates a TCP/IP socket, binds it to the port and listen on it
		"""

		# creation of a TCP/IP socket
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

		server_address = (TRACKER_IP, TRACKER_PORT)

		print('\nTracker starting up on {} port {}\n'.format(*server_address))

		# binding the socket
		self.sock.bind(server_address)

		# listening for incoming connections
		self.sock.listen(BACKLOG)


	def initAesPrev(self,connection):
		"""
		Initiation of the aes key with the previous node
		"""

		message = connection.recv(MESSAGE_SIZE)
		message = message.decode(ENCODING_TYPE,'surrogatepass')

		# reception of the public key
		public_key_prev = [0,0]
		public_key_prev[0] = int(message.split()[1][1:-1]) # Puisqu'on a qqch du type:
		public_key_prev[1] = int(message.split()[2][:-1])  #        KEY (xxxxx, xxxxx)

		# generation of k1
		k1 = aes.generateCipherKey()
		message = matrixToString(k1)
		message = rsa.encryptString(message, public_key_prev )
		connection.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# reception of k2
		k2 = connection.recv(MESSAGE_SIZE)
		k2 = k2.decode(ENCODING_TYPE,'surrogatepass')
		k2 = rsa.decryptString(k2, self.public_key, self.private_key )
		
		# sending of the confirmation and verification
		message = rsa.encryptString(k2, public_key_prev )
		connection.send(message.encode(ENCODING_TYPE,'surrogatepass'))

		# reception of the confirmation and verification
		message = connection.recv(1)
		if message == b'0':
			return initAesPrev()
		
		# XOR
		k2 = stringToMatrix(k2,4,4)

		key = xorTwoMatrices(k1,k2)

		return key


	def handlePeerRequest(self, client, client_address):
		"""
		Handle peer requests, the sending of available files 
		through the network and the the sending of a desired file
		"""
		
		# connection handling : reception of peer files
		encoded_files = client.recv(MESSAGE_SIZE)
		decoded_files = encoded_files.decode(ENCODING_TYPE, 'surrogatepass')
		decoded_files = rsa.decryptString(decoded_files,TRACKER_PUBLIC_KEY,self.private_key)

		previous_node_port =  decoded_files[:1]
		decoded_files = decoded_files[1:]

		print("Received owned files from {} : {} ...".format(previous_node_port, decoded_files))
		
		list_of_files = decoded_files.split(" ")

		for peer_file in list_of_files:

			# check if the file already exist
			if peer_file not in [f for files in self.available_files.values() for f in files]:

				# if it's not a first connection
				if previous_node_port in self.available_files:
					self.available_files[previous_node_port].append(peer_file)
				else:
					self.available_files[previous_node_port] = [peer_file]

		# sending the list of all the files available through the network
		list_of_available_files = ""

		for available_file in [f for files in self.available_files.values() for f in files]:

			list_of_available_files += available_file + " "

		list_of_available_files = list_of_available_files.strip()

		client.send(list_of_available_files.encode(ENCODING_TYPE,'surrogatepass'))

		print("Sending available files : {} ...".format(list_of_available_files))

		aes_key = self.initAesPrev(client)

		print("AES key exchanged")

		# wait for file download request
		request = client.recv(MESSAGE_SIZE)

		if request and request.decode(ENCODING_TYPE, "surrogatepass") != "Disconnect":
			request = request.decode(ENCODING_TYPE,'surrogatepass')
			request = aes.decrypt(request,aes_key)

			for peer, files in self.available_files.items():
				if request in files:
					
					string = "CONNECT TO {} {}".format(str(peer), request)
					print(string)

					string = aes.encrypt(string, aes_key)
					client.send(string.encode(ENCODING_TYPE, 'surrogatepass'))

		print("Closed one connection")
		client.shutdown(socket.SHUT_RDWR)
		client.close()


if __name__ == "__main__":

    if (len(sys.argv)) < 1:
        
        print("Usage : python tracker.py <private key>")

    else:

        tracker = Tracker(int(sys.argv[1]))