from random import randint


def coprime(a, b):
	""" 
	Checks if two numbers are co-primes 

	:param a: the first number
	:param b: the second number
	:return: true if the two numbers are comprime else false
	"""

	while a != 0 and b !=0 :

		if a > b:
			a = a % b
		else:
			b = b % a

	return max(a,b) == 1


def modularInverse(a, m):
	
	""" 
	Computes the modular inverse of a number a in the base m

	:param a: the number
	:param m: the base
	:return: the modular inverse of the number
	"""

	for i in range(2,m):

		if (a*i) % m == 1:
			return i
			

def primeNumber(mmax, mmin=0):
	""" Computes the list of prime numbers between 
	[mmin,mmax] using the algorithm Sieve of Eratosthenes

	:param mmax: the upper bound
	:param mmin: the lower bound
	:return: the list of prime numbers
	"""

	ll = []
	primes = []
	
	for i in range(2,mmax+1):
		ll.append(i)

	while len(ll) > 0:

		tmp = ll[0]
		primes.append(tmp)
		i = 0

		while i < len(ll):

			if ll[i] % tmp == 0:
				ll.remove(ll[i])
				i -= 1
			i += 1
	i = 0
	while not((mmin+i) in primes):
		i += 1

	index = primes.index(mmin + i) 

	return primes[index:]


def keygen():
	""" 
	Generates a public and a private key for RSA

	:return: the public and private key
	"""

	primes = primeNumber(315,100) 
	p = primes[randint(0,len(primes)-1)]
	q = primes[randint(0,len(primes)-1)]

	while q == p:
		q = primes[randint(0,len(primes)-1)]
	
	n = p*q
	phi = (p-1)*(q-1)
	e = randint(2,phi-1)

	while not coprime(e,phi):
		e = randint(2,phi-1)

	d = modularInverse(e,phi)

	return ((n,e),d)


def encrypt(msg, public_key):
	""" 
	Encrypts a message with RSA using the public key

	:param msg: the message to encrypt
	:param public_key: the public key
	:return: the encrypted message
	"""

	return (msg**public_key[1]) % public_key[0]


def decrypt(msg, public_key, private_key):
	""" 
	Decrypts a message with RSA using the public and private key 

	:param msg: the message to decrypt
	:param public_key: the public key
	:param private_key: the private key
	:return: the decrypted message
	"""

	return (msg**private_key) % public_key[0]


def encryptFile(path1,path2,public_key):
	""" 
	Encrypts the file path1 and writes the result in path2
	:param path1: the file to encrypt 
	:param path2: the file where write the result of the encryption
	:param public_key: the public key
	"""

	fin = open(path1,'rb')
	fout= open(path2,'wb')

	buff = fin.read(1)
	while buff != b"":

		buff = int.from_bytes(buff,"little")
		to_write = encrypt(buff,public_key)
		fout.write(to_write.to_bytes(2, byteorder='little'))
		buff = fin.read(1)

	fin.close()
	fout.close()


def decryptFile(path1, path2, public_key, private_key):
	""" 
	Decrypts the file path1 and writes the result in path2

	:param path1: the file to encrypt 
	:param path2: the file where write the result of the decryption
	:param public_key: the public key
	:param private_key: the private key
	"""

	fin = open(path1,'rb')
	fout= open(path2,'wb')

	buff = fin.read(2)
	while buff != b"":

		buff = int.from_bytes(buff,"little")
		to_write = decrypt(buff,public_key,private_key)
		fout.write(to_write.to_bytes(1, byteorder='little'))
		buff = fin.read(2)

	fin.close()
	fout.close()


def encryptString(message, public_key):
	""" 
	Encrypts a message and returns the encrypted message 

	:param message: the message to encrypt
	:param public_key: the public key
	:return: the encrypted message
	"""

	encrypted_message = ""

	for character in message:

		encrypted_character = encrypt(ord(character),public_key)
		encrypted_message += chr(encrypted_character)

	return encrypted_message


def decryptString(msg, public_key, private_key):
	""" 
	Decrypts a message and returns the decrypted message 

	:param msg: the message to decrypt 
	:param public_key: the public key
	:param private_key: the private key
	"""

	decrypted_message = ""
	
	for character in msg:

		decrypted_character = decrypt(ord(character),public_key,private_key)
		decrypted_message += chr(decrypted_character)

	return decrypted_message 